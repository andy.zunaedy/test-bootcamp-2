const data = require('fs');

const loadNote = () => {
    try {
        const dataBuffer = data.readFileSync("data.json");
        const dataJson = dataBuffer.toString();
        return JSON.parse(dataJson);
    } catch (error) {
        return [];
    }
};

function findElectronicalItems() {
    const data = loadNote();
    let str = []
    data.forEach((items) => {
        if (items.type == 'electronic') {
            str.push(items)
        }
    })
    return str
}

console.log(findElectronicalItems());