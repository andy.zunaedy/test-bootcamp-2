const data = require('fs');

const loadNote = () => {
    try {
        const dataBuffer = data.readFileSync("data.json");
        const dataJson = dataBuffer.toString();
        return JSON.parse(dataJson);
    } catch (error) {
        return [];
    }
};

function findItemsMeeting() {
    const data = loadNote();
    let str = []
    data.forEach((items) => {
        if (items.placement.name === "Meeting Room")
            str.push(items)
    })
    return str
}

console.log(findItemsMeeting())








