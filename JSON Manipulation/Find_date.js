const data = require('fs');

const loadNote = () => {
    try {
        const dataBuffer = data.readFileSync("data.json");
        const dataJson = dataBuffer.toString();
        return JSON.parse(dataJson);
    } catch (error) {
        return [];
    }
};

function purchasedOn(date){
        const data = loadNote();
        let str = []
        data.forEach((items) => {
            var purchased_at = new Date(items.purchased_at * 1000).toISOString().substring(0,10)
            if (purchased_at === date) {
                str.push(items)
            }
        })
        return str
    }
    
    console.log(purchasedOn("2020-01-16"));