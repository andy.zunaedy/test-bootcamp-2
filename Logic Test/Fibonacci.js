function Fibonacci(num)
{
    
    if (num == 0) {

        console.log(0)
        return;
    }

    let first = 0, second = 1;

    
    let third = first + second;

 
    while (third <= num) {
 
      
        first = second;
 
       
        second = third;
 
      
        third = first + second;
    }

    let ans = (Math.abs(third - num)
               >= Math.abs(second - num))
                  ? second
                  : third;

    console.log(ans - num);
}

var array = [15,1,3];
var total = 0;

for (var i = 0; i <array.length; i++) {
    total += array[i];
    
}
Fibonacci(total);
    

