function Palidndrome(string) {
    var re = /[\W_]/g;
    var lowString = string.toLowerCase().replace(re, "");
    var reversString = lowString.split("").reverse().join("");
    return reversString === lowString;
  }
  console.log("Hasil Palindrome");
  console.log(Palidndrome("Radar"));
  console.log(Palidndrome("Malam"));
  console.log(Palidndrome("Kasur ini rusak"));
  console.log(Palidndrome("Ibu Ratna antar ubi"));
  console.log(Palidndrome("Malas"));
  console.log(Palidndrome("Makan nasi goreng"));
  console.log(Palidndrome("Balonku ada lima"));